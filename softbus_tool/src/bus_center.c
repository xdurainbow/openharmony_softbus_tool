/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "bus_center.h"

#include "common.h"
#include "softbus_adapter_crypto.h"
#include "softbus_bus_center.h"
#include "softbus_utils.h"

#define SHORT_UDID_HASH_LEN 4
#define SHORT_DEVID_LEN 8

static sem_t g_joinLnnSem;
static sem_t g_leaveLnnSem;

static void OnJoinLNNResultCb(ConnectionAddr *addr, const char *networkId, int32_t retCode)
{
    printf(">>>OnJoinLNNResult networkId = %s, retCode = %d.\n", networkId, retCode);
    sem_post(&g_joinLnnSem);
}

static void OnLeaveLNNDone(const char *networkId, int retCode)
{
    printf(">>>OnLeaveLNNDone networkId = %s, retCode = %d.\n", networkId, retCode);
    sem_post(&g_leaveLnnSem);
}

void BC_JoinLNN(void)
{
    ConnectionAddr addr;
    (void)memset_s(&addr, sizeof(ConnectionAddr), 0, sizeof(ConnectionAddr));
    addr.type = GetInputNumber("Please input connection type(0 - WiFi, 1 - BR, 2 - BLE, 3 - ETH):");
    switch (addr.type) {
        case CONNECTION_ADDR_WLAN:
        case CONNECTION_ADDR_ETH:
            GetInputString("Please input ip:", addr.info.ip.ip, IP_STR_MAX_LEN);
            addr.info.ip.port = GetInputNumber("Please input port:");
            break;
        case CONNECTION_ADDR_BR:
            GetInputString("Please input BT mac:", addr.info.br.brMac, BT_MAC_LEN);
            break;
        case CONNECTION_ADDR_BLE:
            GetInputString("Please input BLE mac:", addr.info.ble.bleMac, BT_MAC_LEN);
            break;
        default:
            printf("unkown connection type, input: %d", addr.type);
            return;
    }
    sem_init(&g_joinLnnSem, 0, 0);
    int ret = JoinLNN(PKG_NAME, &addr, OnJoinLNNResultCb);
    if (ret != 0) {
        printf("JoinLNN fail, ret = %d\n", ret);
        return;
    }
    sem_wait(&g_joinLnnSem);
    sem_destroy(&g_joinLnnSem);
}

void BC_LeaveLNN(void)
{
    char networkId[NETWORK_ID_BUF_LEN];
    GetInputString("Please input network Id:", networkId, NETWORK_ID_BUF_LEN);
    sem_init(&g_leaveLnnSem, 0, 0);
    int ret = LeaveLNN(PKG_NAME, networkId, OnLeaveLNNDone);
    if (ret != 0) {
        printf("LeaveLNN fail, ret = %d\n", ret);
        return;
    }
    sem_wait(&g_leaveLnnSem);
    sem_destroy(&g_leaveLnnSem);
}

static void PrintDeviceInfo(const NodeBasicInfo *info)
{
    printf(">>>DeviceName: %s\n", info->deviceName);
    printf(">>>DeviceNetworkId: %s\n", info->networkId);
    unsigned char udid[UDID_BUF_LEN] = {0};
    int ret = GetNodeKeyInfo(PKG_NAME, info->networkId, NODE_KEY_UDID, udid, UDID_BUF_LEN);
    if (ret != 0) {
        printf("GetNodeKeyInfo NODE_KEY_UDID fail, ret = %d.\n", ret);
        return;
    }
    printf(">>>Udid: %s\n", udid);
    unsigned char uuid[UUID_BUF_LEN] = {0};
    ret = GetNodeKeyInfo(PKG_NAME, info->networkId, NODE_KEY_UUID, uuid, UUID_BUF_LEN);
    if (ret != 0) {
        printf("GetNodeKeyInfo NODE_KEY_UUID fail, ret = %d.\n", ret);
        return;
    }
    printf(">>>Uuid: %s\n", uuid);

    /* calculate udid hash for ble discovery */
    unsigned char udidHash[UDID_HASH_LEN] = {0};
    char shortDevId[SHORT_DEVID_LEN + 1] = {0};
    (void)SoftBusGenerateStrHash(udid, strlen((const char*)udid) + 1, udidHash);
    (void)ConvertBytesToHexString(shortDevId, sizeof(shortDevId), udidHash, SHORT_UDID_HASH_LEN);
    printf(">>>ShortDevId: %s\n", shortDevId);
}

void BC_GetOnlineDeviceInfo(void)
{
    int32_t onlineNum;
    NodeBasicInfo *info = NULL;
    int ret = GetAllNodeDeviceInfo(PKG_NAME, &info, &onlineNum);
    if (ret != 0) {
        printf("GetAllNodeDeviceInfo fail, ret = %d.\n", ret);
        return;
    }
    printf("online devices num: %d\n", onlineNum);
    for (int i = 0; i < onlineNum; i++) {
        printf(">>>Index: %d\n", i);
        PrintDeviceInfo(&info[i]);
    }
    FreeNodeInfo(info);
}

void BC_GetLocalDeviceInfo(void)
{
    NodeBasicInfo info;
    int ret = GetLocalNodeDeviceInfo(PKG_NAME, &info);
    if (ret != 0) {
        printf("GetLocalNodeDeviceInfo fail, ret = %d.\n", ret);
        return;
    }
    PrintDeviceInfo(&info);
}
